import { StudentModel } from "../models/StudentModel";
import { BaseStudent, Student } from '../interfaces';

interface CommonReturn {
    status: boolean,
    message: string;
    data: Student | null;
}

interface GetAllReturn {
    status: boolean,
    message: string;
    data: Student[] | []
}

export const insertBulkStudent = async (collegeData: BaseStudent[]): Promise<CommonReturn> =>{
    try {
       const result =  await StudentModel.insertMany(collegeData);
       console.log('insertBulkStudent Resilt=>', result);
       if (result) {
        return <CommonReturn> {
            status: true,
            message: "Data inserted"
        }
       } else {
        return <CommonReturn> {
            status: false,
            message: "Error while inserting data."
        } 
       }
        
    } catch (error: any) {
        console.error('insertBulkStudent Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const getAllStudents = async () => {
    try {
       const result = await StudentModel.find();  
       return <GetAllReturn> {
            status: true,
            message: "Data fetched",
            data: result
        }
       
    } catch (error: any) {
        console.error('getAllStudents Error =>', error);
        return <GetAllReturn> {
            status: false,
            message: error?.message,
            data: []
        } 
    }
}

export const updateStudentById = async (id: string, updateChangeSet: Partial<BaseStudent>) => {
    try {
        const result = await StudentModel.updateOne({_id: id}, updateChangeSet);
        if (result) {
            return <CommonReturn> {
                status: true,
                message: "Data updated"
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: "Error while updating data."
            } 
        }
    } catch (error: any) {
        console.error('updateStudentById Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }

}

export const insertOneStudent = async (studentData: BaseStudent): Promise<CommonReturn> => {
    try {
        const result = await StudentModel.create(studentData);
        if (result) {
            return <CommonReturn> {
                status: true,
                message: "Data inserted",
                data: result
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: "Error while inserting data."
            } 
        }
    } catch (error: any) {
        console.error('insertOneStudent Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const getStudentsByCollegeId = async (collegeId: string): Promise<GetAllReturn> => {
    try {
        const result = await StudentModel.find({collegeId});
        if (result) {
            return <GetAllReturn> {
                status: true,
                message: "Data fetched",
                data: result
            }
        } else {
            return <GetAllReturn> {
                status: false,
                message: "Error while fetching student data."
            } 
        }
    } catch (error: any) {
        console.error('getStudentsByCollegeId Error =>', error);
        return <GetAllReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const findOneStudent = async (studentId: string): Promise<CommonReturn> => {
    try {
        const result = await StudentModel.findById(studentId);
        if (result) {
            return <CommonReturn> {
                status: true,
                message: "Data fetched",
                data: result
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: "Error while fetching data."
            } 
        }
    } catch (error: any) {
        console.error('findOneStudent Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}
