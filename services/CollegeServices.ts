import { CollegeModel } from "../models/CollegeModel";
import { BaseCollege, College } from '../interfaces';
import { StaticData, capitalize } from "../utils";

interface CommonReturn {
    status: boolean,
    message: string;
    data: College | null
}

interface GetAllReturn {
    status: boolean,
    message: string;
    data: College[]
}

interface StatsReturn {
    status: boolean,
    message: string;
    data: any
}

export const insertBulkCollege = async (collegeData: BaseCollege[]): Promise<CommonReturn> =>{
    try {
       const result =  await CollegeModel.insertMany(collegeData);
       console.log('insertBulkCollege Resilt=>', result);
       if (result) {
        return <CommonReturn> {
            status: true,
            message: "Data inserted"
        }
       } else {
        return <CommonReturn> {
            status: false,
            message: "Error while inserting data."
        } 
       }
        
    } catch (error: any) {
        console.error('insertBulkCollege Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const getAllColleges = async () => {
    try {
       const result = await CollegeModel.find();  
       return <GetAllReturn> {
            status: true,
            message: "Data fetched",
            data: result
        }
       
    } catch (error: any) {
        console.error('getAllColleges Error =>', error);
        return <GetAllReturn> {
            status: false,
            message: error?.message,
            data: []
        } 
    }
}

export const updateCollegeById = async (id: string, updateChangeSet: Partial<BaseCollege>) => {
    try {
        const result = await CollegeModel.updateOne({_id: id}, updateChangeSet);
        if (result) {
            return <CommonReturn> {
                status: true,
                message: "Data updated"
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: "Error while updating data."
            } 
        }
    } catch (error: any) {
        console.error('updateCollegeById Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }

}

export const insertOneCollege = async (collegeData: BaseCollege): Promise<CommonReturn> =>{
    try {
       const result =  await CollegeModel.create(collegeData);
       console.log('insertOneCollege Resilt=>', result);
       if (result) {
        return <CommonReturn> {
            status: true,
            message: "Data inserted",
            data: result
        }
       } else {
        return <CommonReturn> {
            status: false,
            message: "Error while inserting data."
        } 
       }
        
    } catch (error: any) {
        console.error('insertOneCollege Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const isCollegeExist = async (keySet: Partial<BaseCollege>): Promise<CommonReturn> => {
    try {
        const result = await CollegeModel.exists(keySet);
        if (result) {
            return <CommonReturn> {
                status: true,
                message: "College exist"
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: "College does not exist."
            } 
        }
    } catch (error: any) {
        console.error('isCollegeExist Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const findOneCollege = async (id: string): Promise<CommonReturn> =>{
    try {
       const result =  await CollegeModel.findById(id);
       console.log('findOneCollege Result=>', result);
       if (result) {
        return <CommonReturn> {
            status: true,
            message: "Got Data",
            data: result
        }
       } else {
        return <CommonReturn> {
            status: false,
            message: "Data not found."
        } 
       }
        
    } catch (error: any) {
        console.error('findOneCollege Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}

export const getCollegeStats = async (): Promise<StatsReturn> => {
    try {
        // 
        let stateWise = await CollegeModel.aggregate([
            { "$group": {
                "_id": { "$toLower": "$stateName" },
                "count": { "$sum": 1 }
            }}
        ]);

        stateWise = stateWise.map((item) => {
            return [capitalize(item._id), item.count];
        })

        // get Course wise data.
        let courseWise = await Promise.all(StaticData.coursesList.map(async (course) => {
           let  count = await CollegeModel.count({
               coursesOffered: { $in: [course]}
            });
            return [course, count];
        }));
    
        return <StatsReturn> {
            status: true,
            message: "StateWise Stats",
            data: {
                stateWise, 
                courseWise
            }
        } 

    } catch (error: any) {
        console.error('getCollegeStats Error =>', error);
        return <StatsReturn> {
            status: false,
            message: error?.message
        }
    }
}
export const updateStudentCount = async (id: string, countUpdate: number) => {
    try {
        const result = await CollegeModel.updateOne(
            {_id: id},
            {$inc: {studentCount: countUpdate}}
        )
        if (result) {
            return <CommonReturn> {
                status: true,
                message: 'Data Updated'
            }
        } else {
            return <CommonReturn> {
                status: false,
                message: 'Fail to update data.'
            }  
        }
    } catch (error: any) {
        console.error('updateStudentCount Error =>', error);
        return <CommonReturn> {
            status: false,
            message: error?.message
        }
    }
}


export const getSimilarColleges = async (cityName: string, stateName:string, courseOffers: string, studentCount: number): Promise<GetAllReturn> => {
    try {
        // create a query
        const query = {
            cityName: cityName,
            stateName: stateName,
            coursesOffered: {
                $in: [courseOffers]
            },
            studentCount: {
                $gte: (studentCount-10),
                $lt: (studentCount+10)
            }
        }

       const result = await CollegeModel.find(query);  
       return <GetAllReturn> {
            status: true,
            message: "Data fetched",
            data: result
        }
       
    } catch (error: any) {
        console.error('getAllColleges Error =>', error);
        return <GetAllReturn> {
            status: false,
            message: error?.message,
            data: []
        } 
    }
}