export interface ErrorObject {
    statusCode: number;
    message: string;
    errorCode: string;
}

export interface ErrorResponse {
    error: ErrorObject;
}



export interface BaseStudent {
    name: string;
    batchYear: string;
    collegeId: string;
    gender: string;
    skills: string[];
	course: string;
}

export interface Student extends BaseStudent {
    _id: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface BaseCollege {
    name:string;
    yearFounded: string;
    coursesOffered: string[];
    cityName: string;
    stateName: string;
    countryName: string;
    studentCount: number;
}

export interface College extends BaseCollege {
    _id: string;
    createdAt: Date;
    updatedAt: Date;
}