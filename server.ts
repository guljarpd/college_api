import 'dotenv/config';
import express, { Request, Response} from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import config from 'config';
import { HttpStatusCode } from './utils';
import { errorHandler } from './middlewares';
import { Router } from './routers';
// import FakeData  from './fake_data';
const app = express();
const PORT: number = config.get("port");
// 
mongoose.connect(config.get("mongoDB_url")).then(()=>{
    console.log('Database Connected');
}).catch((err) =>{
    console.error(err);
});
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));


// if(process.env.ADD_FAKE_DATA){
//     FakeData();
// }

// 
app.use(cors());
app.use(express.json());
app.use(Router);
app.use(errorHandler);

// Base and fallback handller.
app.get('/', (_req: Request, res: Response) => {
    console.log('Base url called.');
    res.status(HttpStatusCode.OK).json({
        data: {
            message: 'College Api.'
        }
    });
});


//  export server port.
app.listen(PORT, async () => {
    console.log('Server Started. Port: '+ PORT);
});





