import { ErrorCodes } from "../utils";
/**
 * Handling Http Exception
 */
export default class HttpExceptions extends Error {
  statusCode?: number;
  message: string;
  errorCode: string;
  //
  constructor(statusCode: number, message: string, errorCode?: string) {
    super(message);
    this.statusCode = statusCode;
    this.message = message;
    this.errorCode = errorCode || ErrorCodes.UNKNOWN_ERROR;
  }
}
