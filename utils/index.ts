export {default as HttpStatusCode} from "./HttpStatusCode";
export {default as ErrorCodes} from "./ErrorCodes";
export * as StaticData from './data';

export const capitalize = (str: string) => {
    const lower = str.toLowerCase();
    return str.charAt(0).toUpperCase() + lower.slice(1);
}
