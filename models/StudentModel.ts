import { Schema, Types, model } from "mongoose";
import { Student } from "../interfaces";

const StudentSchema = new Schema<Student>({
    collegeId: {type: String, required: true},
    name:{type: String, required: true},
    batchYear: {type: String, required: true},
    skills: {type: [String], required: true},
    course: {type: String, required: true},
    gender: {type: String, required: true}

},{
    timestamps: true
});
// 
export const StudentModel =  model('students', StudentSchema);