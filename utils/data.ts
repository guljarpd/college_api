export const coursesList: string[] = [
	"Architectural Engineering",
	"Civil Engineering",
	"Electrical Engineering",
	"Mechanical engineering",
	"Information Technology",
	"Computer Science Engineering",
	"Agriculture Engineering",
	"Aerospace Engineering",
	"Biomedical Engineering",
];