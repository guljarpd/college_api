
/**
 * All custom error codes should define here.
 */
enum ErrorCodes {
    NOT_FOUND = 'NOT_FOUND',
    UNKNOWN_ERROR = 'UNKNOWN_ERROR',
    COLLEGE_NOT_FOUND = 'COLLEGE_NOT_FOUND',
    STUDENT_NOT_FOUND = 'STUDENT_NOT_FOUND',
    INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR',
    INVALID_PARAMS = 'INVALID_PARAMS',
    EXIST = 'EXIST',
}

// 
export default ErrorCodes;