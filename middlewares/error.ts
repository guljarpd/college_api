import { Request, Response, NextFunction } from 'express';
import HttpExceptions from './http_exceptions';

export default (error: HttpExceptions, _request: Request, response: Response, _next: NextFunction) => {
  const status = error.statusCode || 500;
  response.status(status).json({ error });
};
