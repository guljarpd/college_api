import { count } from "console";
import { Request, Response } from "express";
import { ErrorResponse, ErrorObject, BaseStudent, Student } from "../interfaces";
import { updateStudentCount } from "../services/CollegeServices";
import { findOneStudent, getStudentsByCollegeId, insertOneStudent } from "../services/StudentServices";
import { HttpStatusCode, ErrorCodes } from "../utils";

interface ResStudent {
    data: Student | Student[]
}

/**
 * Add a student in db.
 * @param req 
 * @param res 
 * @returns 
 */
export const AddStudent = async (req: Request, res: Response<ResStudent | ErrorResponse>) => {
    try {
        const payload: BaseStudent = req.body;
        const student = await insertOneStudent(payload);
        if (student.status && student.data) {
            // Increment by [1] studentCount for this college.
            await updateStudentCount(payload.collegeId, 1);
            // 
            return res.status(HttpStatusCode.CREATED).json({
                data: student.data
            });
        } else {
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.INVALID_PARAMS,
                statusCode: HttpStatusCode.BAD_REQUEST,
                message: student?.message
            }
            // 
            return res.status(HttpStatusCode.BAD_REQUEST).json({
                error: errorObj
            }) 
        }
    } catch (error: any) {
        console.error('AddCollege Error =>', error);
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
            statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
            message: error?.message
        }
        // 
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            error: errorObj
        }); 
    }
}

/**
 * Get All student by College Id.
 * @param req 
 * @param res 
 * @returns 
 */
export const GetStudentByCollegeId = async (req: Request, res: Response<ResStudent | ErrorResponse>) => {
    try {
        const collegeId: string = req.params?.id;
        const students = await getStudentsByCollegeId(collegeId);
        if (students.status && students.data) {
            return res.status(HttpStatusCode.OK).json({
                data: students.data
            });
        } else {
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.STUDENT_NOT_FOUND,
                statusCode: HttpStatusCode.NOT_FOUND,
                message: students?.message
            }
            // 
            return res.status(HttpStatusCode.NOT_FOUND).json({
                error: errorObj
            })  
        }
    } catch (error: any) {
        console.error('GetStudentByCollegeId Error =>', error);
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
            statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
            message: error?.message
        }
        // 
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            error: errorObj
        }); 
    }
}

/**
 * Find Student details by Id.
 * @param req 
 * @param res 
 * @returns 
 */
export const GetStudentDetails = async (req: Request, res: Response<ResStudent | ErrorResponse>) => {
    try {
        const studentId: string = req.params?.id;
        const student = await findOneStudent(studentId);
        if (student.status && student.data) {
            return res.status(HttpStatusCode.OK).json({
                data: student.data
            });
        } else {
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.STUDENT_NOT_FOUND,
                statusCode: HttpStatusCode.NOT_FOUND,
                message: student?.message
            }
            // 
            return res.status(HttpStatusCode.NOT_FOUND).json({
                error: errorObj
            })  
        }
    } catch (error: any) {
        console.error('GetStudentDetails Error =>', error);
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
            statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
            message: error?.message
        }
        // 
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            error: errorObj
        }); 
    }
}


