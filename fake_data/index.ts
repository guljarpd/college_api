import faker from 'fakerator';
import { College } from '../interfaces';
import { getAllColleges, insertBulkCollege, updateCollegeById } from '../services/CollegeServices';
import { insertBulkStudent } from '../services/StudentServices';
const collegeList: string[] = [
	"Aazad College of Education",
	"Abhinav Institute of Management & Technology",
	"A.B.M. College",
	"A.B.R College of Education",
	"A.C.College",
	"A.C.Evening College",
	"Acharya N.G.Ranga College of Education",
	"Adarsa College of Education",
	"A.G.K.M.College",
	"A.L.College of Education",
	"AL Momin College of Education",
	"Alpha College of Education",
	"Alpha Degree College",
	"Aman Showkath B.Ed College",
	"A.M.G.College of Education",
	"A.M Reddy College of Education",
	"Amrutha Degree College",
	"Andhra Kesari College of Education",
	"Anitha Venkateswara College of Eduation",
	"Annabattuni Satyanarayana Degree College",
	"Anniebesant College of Education",
	"ANU PG Campus Ongole",
	"A.P.Residential Degree College",
	"Asha College of Education",
	"B.A & K.R College of Education",
	"B.A & K.R Degree College",
	"Balaji Degree College",
	"Bapatla Engineering College",
	"Bapuji College of Education",
	"B.B.H Degree College",
	"B.G Degree College",
	"Bharathi Degree College",
	"Bhashyam College of Education",
	"BMRM Jhansi B.Ed College",
	"B.R College of Education",
	"B.S.S.B College",
	"B.V.S.P.M Degree College",
	"Career Degree College",
	"Chaitanya Bharathi Degree College",
	"Chaitanya College of Education",
	"Chakradhar Degree College",
	"Chalapathi Institute of Engineering & Technology",
	"Chebrolu Hanumaiah College of Sciences",
	"Chegireddy Linga Reddy Instt of Management",
	"Chirala College of PG Studies",
	"Christu Jayanthi Jubilee College",
	"C.I.L.For Rural Management",
	"C.R. College",
	"C.S.R. Sarma College",
	"C.R.M Degree College",
	"Devathi Venkata Subbaiah Degree College",
	"Don Bosco College",
	"Dr. B.R.Ambedkar Centenary Degree College",
	"Dr.Homi Jahangir Baba Degree College",
	"Dr. Jayapradamba Degree College",
	"Dr. K.R.R.M. Degree College",
	"Dr. K.S.P.R College of Education",
	"Dr.K.V.K. Sanskrit College",
	"D.R.N.S.C.V.S. College",
	"Dr. Zakir Hussain College of Education",
	"Erigineni Tirupati Naidu & Lakshmamma Degree College",
	"G.C.K.V.N. Degree College",
	"G.C & Y P N Degree College",
	"Geetham Degree College",
	"Geethanjali Degree College",
	"Goutham College of Education",
	"Government Degree College",
	"Government Engg College",
	"Govt. Degree College",
	"Govt. Engg College",
	"Govt. Technology College",
	"G.R.K Degree College",
	"G.R.R. & T.P.R. Degree College",
	"G.S.R. Degree College",
	"Assam Science and Technology",
	"Ashoka University",
	"Aryabhatta Knowledge Park",
	"CEPT University",
	"Centurion University",
	"Baba Ghulam Shah Badhshah University",
	"Baddi University",
	"Banaras Hindu University",
	"Banasthali Vidyapith",
	"Banda University",
	"Bareilly International University",
	"Bhupal Nobles University",
	"Bidhan Chandra Krishi Vishwavidyalaya",
	"Bihar Agricultural University",
	"Biju Patnaik University of Technology",
	"Binod Bihari Mahto Koyalanchal University",
	"Birla Institute of Technology",
	"Birsa Agricultural University",
	"BLDE University",
	"BML Munjal University",
	"India Institute Technology",
	"India Engg College",
	"Amity University",
	"Amrita University",
	"Andhra University",
	"Anna University",
  ];

interface Locations {
    countryName: string;
    cityName: string;
    stateName: string;
}

const locationsList: Locations[] = [
    {
        countryName: "India",
        cityName: "Mumbai",
        stateName: "Maharashtra",
    },
    {
        countryName: "India",
        cityName: "Delhi",
        stateName: "Delhi",
    },
    {
        countryName: "India",
        cityName: "Bengaluru",
        stateName: "Karnataka",
    },
    {
        countryName: "India",
        cityName: "Chennai",
        stateName: "Tamilnadu",
    },
    {
        countryName: "India",
        cityName: "Hyderabad",
        stateName: "Telangana",
    }
];
//
const coursesList: string[] = [
	"Architectural Engineering",
	"Civil Engineering",
	"Electrical Engineering",
	"Mechanical engineering",
	"Information Technology",
	"Computer Science Engineering",
	"Agriculture Engineering",
	"Aerospace Engineering",
	"Biomedical Engineering",
];

// skills
const skillsList: string[] = [
	"C",
	"C++",
	"Java",
	"Nodejs",
	"Python",
	"Elixir",
	"HTML",
	"CSS",
	"PostgreSQL",
	"MySQL",
	"MongoDB",
	"Redis",
	"GCP",
	"AWS",
	"Digital Ocean",
	"DevOps",
	"Docker"
]
const genderList: string[] = ["Male", "Female"];
// 
const yearFounded: string[] = ["1980", "1985", "1988", "1990", "1992", "1995", "1998", "2000"];
const yearBatch: string[] = ["2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015"];
//   
const getMultipleItemsFromList = (dataList: string[], maxLength: number) => {
	let newCourses: string[] = [];
	for (let i = 0; i < maxLength; i++) {
		let item: string = getItemFromList(dataList);
		if (!newCourses.includes(item)) newCourses.push(item);
	}
	return newCourses;
};
// 
const getItemFromList = (dataList: string[]) => {
	let indx = Math.floor(Math.random() * (dataList.length - 1));
	return dataList[indx];
}
const getLocationItemFromList = (dataList: Locations[]) => {
	let indx = Math.floor(Math.random() * (dataList.length - 1));
	return dataList[indx];
}

interface CollegeObj {
    name: string;
    yearFounded: string;
    coursesOffered: string[];
    cityName: string;
    stateName: string;
    countryName: string;
    studentCount: number;
}
// 
const makeCollegeObj = (collegeName: string) => {
	const location: Locations = getLocationItemFromList(locationsList);
	// 
	const collegeObj: CollegeObj = {
		name: collegeName,
		yearFounded: getItemFromList(yearFounded),
		coursesOffered: getMultipleItemsFromList(coursesList, 7),
		cityName: location?.cityName,
		stateName: location?.stateName,
		countryName: location?.countryName,
		studentCount: 0 // initial it will be zero.
	};

	return collegeObj;
}
// 
const finalCollegeList = () => {
	const finalList = [];
	for (let i = 0; i < collegeList.length; i++) {
		finalList.push(makeCollegeObj(collegeList[i]));
	}
	return finalList;
}
// 
const bulkInsertCollege = () =>{
	insertBulkCollege(finalCollegeList()).then(res=>{
		console.log('bulkInsertCollege res=>', res);
	}).catch(err =>{
		console.error('bulkInsertCollege err=>', err);
	})
}
interface StudentObj {
    name: string;
    batchYear: string;
    collegeId: string;
    gender: string;
    skills: string[];
	course: string;
}
// 
const makeStudentObj = (studentName: string, collegeId: string, gender: string, course: string) => {
	const studentObj: StudentObj = {
		name: studentName,
		batchYear: getItemFromList(yearBatch),
		collegeId: collegeId,
		gender: gender,
		course: course,
		skills: getMultipleItemsFromList(skillsList, 10)
	}
	// 
	return studentObj;
}

const makeStudentsList = (collegeId: string, collegeCourses: string[], studentCount: number) => {
	const finalStudentsList: StudentObj[] = [];
	for (let i = 0; i < studentCount; i++) {
		let fakeData = faker();
		let gender = getItemFromList(genderList);
		let fullName = '';
		if (gender === 'Female') {
			fullName = `${fakeData.names.firstNameF()} ${fakeData.names.lastNameF()}`
		} else {
			fullName = `${fakeData.names.firstNameM()} ${fakeData.names.lastNameM()}`
		}
		// 
		finalStudentsList.push(makeStudentObj(fullName, collegeId,gender, getItemFromList(collegeCourses)));
	}
	// 
	return finalStudentsList;
	
}

const main = async () => {
	// bulkInsertCollege();
	// const studentCountList = ["90", "100", "110", "120"];
	// const allColleges = await getAllColleges();
	// console.log('allColleges =>', allColleges);
	// if (allColleges.status) {
	// 	allColleges.data.forEach( async (item: College) =>{
	// 		let studentCount = getItemFromList(studentCountList)
	// 		let finalStudentList = makeStudentsList(item._id, item.coursesOffered, parseInt(studentCount));
	// 		// insert student for each college.
	// 		await insertBulkStudent(finalStudentList);
	// 		await updateCollegeById(item._id, {studentCount: parseInt(studentCount)});
	// 	});
	// }
	
}
export default main;





