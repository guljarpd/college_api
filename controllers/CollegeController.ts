import { Request, Response } from "express";
import { ErrorResponse, ErrorObject, BaseCollege, College } from "../interfaces";
import { findOneCollege, getAllColleges, getCollegeStats, getSimilarColleges, insertOneCollege, isCollegeExist } from "../services/CollegeServices";
import { HttpStatusCode, ErrorCodes } from "../utils";

interface ResCollege {
    data: College | College[]
}

interface ResDashboard {
    data: any;
}

/**
 * Add a new college entry here.
 * @param req 
 * @param res 
 */
export const AddCollege = async (req: Request, res: Response<ErrorResponse | ResCollege>) => {
    try {
        const payload: BaseCollege = req.body;
        let isExist = await isCollegeExist({
            name: payload?.name,
            yearFounded: payload?.yearFounded,
            cityName: payload?.cityName,
            stateName: payload.stateName,
            countryName: payload.countryName
        });
        if (isExist.status) {
            // allready college entry exist
            console.log('AddCollege =>', 'Allready college entry exist');
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.EXIST,
                statusCode: HttpStatusCode.BAD_REQUEST,
                message: 'Allrady this college exist.'
            }
            // 
            return res.status(HttpStatusCode.BAD_REQUEST).json({
                error: errorObj
            })
        } 
        // 
        let college = await insertOneCollege(payload);
        console.log('AddCollege College =>', college);
        // 
        if (college.status && college.data) {
            return res.status(HttpStatusCode.CREATED).json({
                data: college.data
            });
        } else {
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.INVALID_PARAMS,
                statusCode: HttpStatusCode.BAD_REQUEST,
                message: college?.message
            }
            // 
            return res.status(HttpStatusCode.BAD_REQUEST).json({
                error: errorObj
            }) 
        }
    } catch (error: any) {
        console.error('AddCollege Error =>', error);
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
            statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
            message: error?.message
        }
        // 
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            error: errorObj
        });
    }
}


/**
 * Get All Colleges
 * @param req 
 * @param res 
 * @returns 
 */
export const GetAllColleges = async (req: Request, res: Response<ErrorResponse | ResCollege>) => {
    try {
        const college = await getAllColleges();
        if (college.status && college?.data) {
         return res.status(HttpStatusCode.OK).json({
             data: college.data
         });
        } else {
         const errorObj: ErrorObject = {
             errorCode: ErrorCodes.NOT_FOUND,
             statusCode: HttpStatusCode.NOT_FOUND,
             message: college.message
         }
         // 
         return res.status(HttpStatusCode.NOT_FOUND).json({
             error: errorObj
         }) 
        }
    } catch (error: any) {
     console.error('GetAllColleges Error =>', error);
     const errorObj: ErrorObject = {
         errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
         statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
         message: error?.message
     }
     // 
     return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
         error: errorObj
     });
    } 
 }

/**
 * Get a college details.
 * @param req 
 * @param res 
 */
export const GetCollegeById = async (req: Request, res: Response<ErrorResponse | ResCollege>) => {
   try {
       const id: string = req.params?.id;
       const college = await findOneCollege(id);
       if (college.status && college?.data) {
        return res.status(HttpStatusCode.OK).json({
            data: college.data
        });
       } else {
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.NOT_FOUND,
            statusCode: HttpStatusCode.NOT_FOUND,
            message: college.message
        }
        // 
        return res.status(HttpStatusCode.NOT_FOUND).json({
            error: errorObj
        }) 
       }
   } catch (error: any) {
    console.error('GetCollegeById Error =>', error);
    const errorObj: ErrorObject = {
        errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
        statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
        message: error?.message
    }
    // 
    return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
        error: errorObj
    });
   } 
}

export const GetCollegeDashboard = async (req: Request, res: Response<ErrorResponse | ResDashboard>) => {
    try {
        const collegeStats = await getCollegeStats();
        if (collegeStats.status && collegeStats?.data) {
         return res.status(HttpStatusCode.OK).json({
             data: collegeStats.data
            //  data: {
            //      stateWise: collegeStats.data,
            //      courseWise: []
            //  }
         });
        } else {
         const errorObj: ErrorObject = {
             errorCode: ErrorCodes.NOT_FOUND,
             statusCode: HttpStatusCode.NOT_FOUND,
             message: collegeStats.message
         }
         // 
         return res.status(HttpStatusCode.NOT_FOUND).json({
             error: errorObj
         }) 
        }
    } catch (error: any) {
     console.error('getCollegeDashboard Error =>', error);
     const errorObj: ErrorObject = {
         errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
         statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
         message: error?.message
     }
     // 
     return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
         error: errorObj
     });
    } 
 }

export const GetSimilarColleges = async (req: Request, res: Response<ErrorResponse | ResDashboard>) => {
    try {
        const cityName = req.params.cityName;
        const stateName = req.params.stateName;
        const courseOffers = req.params.courseOffers; 
        const studentCount = parseInt(req.params.studentCount);

        const colleges = await getSimilarColleges(cityName, stateName, courseOffers, studentCount);
        
        if (colleges.status && colleges?.data) {
            return res.status(HttpStatusCode.OK).json({
                data: colleges.data
            });
        } else {
            const errorObj: ErrorObject = {
                errorCode: ErrorCodes.NOT_FOUND,
                statusCode: HttpStatusCode.NOT_FOUND,
                message: colleges.message
            }
            // 
            return res.status(HttpStatusCode.NOT_FOUND).json({
                error: errorObj
            });
        }
    } catch (error: any) {
        console.error('GetSimilarColleges Error =>', error);
        const errorObj: ErrorObject = {
            errorCode: ErrorCodes.INTERNAL_SERVER_ERROR,
            statusCode: HttpStatusCode.INTERNAL_SERVER_ERROR,
            message: error?.message
        }
        // 
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            error: errorObj
        });
    }
}