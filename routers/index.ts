import express from 'express';
export const Router = express.Router();

import { CollegeController, StudentController } from '../controllers';


// College APIs
Router.post('/api/v1/college/add', CollegeController.AddCollege);
Router.get('/api/v1/college/all', CollegeController.GetAllColleges);
Router.get('/api/v1/college/details/:id', CollegeController.GetCollegeById);
Router.get('/api/v1/college/students/:id', StudentController.GetStudentByCollegeId);
Router.get('/api/v1/college/similar/:cityName/:stateName/:courseOffers/:studentCount', CollegeController.GetSimilarColleges);

// Student APIs
Router.post('/api/v1/student/add', StudentController.AddStudent);
Router.get('/api/v1/student/details/:id', StudentController.GetStudentDetails);


// Dashboard APIs
Router.get('/api/v1/college/dashboard', CollegeController.GetCollegeDashboard);