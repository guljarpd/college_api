import { Schema, Types, Model, model } from "mongoose";
import { College } from "../interfaces";

const CollegeSchema = new Schema<College>({
    name:{type: String, required: true},
    yearFounded: {type: String, required: true},
    coursesOffered: {type: [String], required: true},
    cityName: {type: String, required: true},
    stateName: {type: String, required: true},
    countryName: {type: String, required: true},
    studentCount: {type: Number, default: 0}

},{
    timestamps: true
});

// 
export const CollegeModel =  model('colleges', CollegeSchema);